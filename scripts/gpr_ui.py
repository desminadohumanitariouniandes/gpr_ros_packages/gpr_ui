#!/usr/bin/env python

import sys
import rospy

from ui_top import RobotUserInterface

from PyQt5.QtWidgets import QApplication

def main():

	# Initialize ROS node
	rospy.init_node('gpr_ui', anonymous=False)

	#while not rospy.is_shutdown():
		
	gpr_ui = QApplication(sys.argv)
		
	view = RobotUserInterface()
	view.show()
		
	sys.exit(gpr_ui.exec_())

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass