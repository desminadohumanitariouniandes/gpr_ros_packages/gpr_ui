

import math as m

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel


from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.ticker import AutoMinorLocator, MultipleLocator


class CoordDisplay(QWidget):

	def __init__(self):

		# Super class initialization
		super(CoordDisplay, self).__init__()

		self.figure = Figure()
		self.canvas = FigureCanvas(self.figure)
		
		# Title label
		lb_title = QLabel("Position Plot")
		lb_title.setAlignment(Qt.AlignCenter)
		lb_title.setStyleSheet(
			'font-size: 18pt; \
			font-family: "DejaVu Sans Mono";\
			color: black;\
			font-style: bold;'
		)

		# Figure
		self.figure.set_facecolor('#A1B3A3')

		ax = self.figure.add_subplot(111)
		ax.set_xlabel("X axis")
		ax.set_ylabel("Y axis")
		ax.set_xlim(0, 80)
		ax.set_ylim(0, 80)
		ax.xaxis.set_major_locator(MultipleLocator(10))
		ax.yaxis.set_major_locator(MultipleLocator(10))
		ax.xaxis.set_minor_locator(AutoMinorLocator(5))
		ax.yaxis.set_minor_locator(AutoMinorLocator(5))
		ax.grid(which='major', linestyle='--')
		ax.grid(which='minor', linestyle=':')
		ax.set_aspect('equal')

		self.figure.tight_layout()
		self.canvas.draw()

		ly_main = QVBoxLayout()
		ly_main.setSpacing(10)
		ly_main.setAlignment(Qt.AlignTop)
		ly_main.addWidget(lb_title, 1)
		ly_main.addWidget(self.canvas, 19)

		self.setLayout(ly_main)


	def update_plot(self, x_coord, y_coord, z_angle):

		ax = self.figure.add_subplot(111)

		ax.clear()

		ax.set_xlabel("Eje X")
		ax.set_ylabel("Eje Y")
		ax.set_xlim(0, 80)
		ax.set_ylim(0, 80)
		ax.xaxis.set_major_locator(MultipleLocator(10))
		ax.yaxis.set_major_locator(MultipleLocator(10))
		ax.xaxis.set_minor_locator(AutoMinorLocator(5))
		ax.yaxis.set_minor_locator(AutoMinorLocator(5))
		ax.grid(which='major', linestyle='--')
		ax.grid(which='minor', linestyle=':')
		ax.set_aspect('equal')

		x_end = 10 * m.cos(z_angle)
		y_end = 10 * m.sin(z_angle)

		ax.scatter([x_coord/10], [y_coord/10], s=100, marker='x')
		ax.arrow(x_coord/10, y_coord/10, x_end, y_end, width=0.1)

		self.canvas.draw()
