

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QLabel, QVBoxLayout, QHBoxLayout, QGridLayout


class CoordInfo(QWidget):

	def __init__(self):

		# Super class initialization
		super(CoordInfo, self).__init__()

		# Non fixed labels configuration
		self.lb_x_coord = QLabel("0.00 cm")
		self.lb_x_coord.setAlignment(Qt.AlignRight)
		self.lb_x_coord.setStyleSheet(
			'font-size: 12pt;\
			font-family: "DejaVu Sans Mono";'
		)

		self.lb_y_coord = QLabel("0.00 cm")
		self.lb_y_coord.setAlignment(Qt.AlignRight)
		self.lb_y_coord.setStyleSheet(
			'font-size: 12pt;\
			font-family: "DejaVu Sans Mono";'
		)

		self.lb_z_angle = QLabel("0.00 deg")
		self.lb_z_angle.setAlignment(Qt.AlignRight)
		self.lb_z_angle.setStyleSheet(
			'font-size: 12pt;\
			font-family: "DejaVu Sans Mono";'
		)

		# Fixed labels
		lb_title = QLabel("Coordinates")
		lb_title.setAlignment(Qt.AlignCenter)
		lb_title.setStyleSheet(
			'font-size: 18pt;\
			font-family: "DejaVu Sans Mono";\
			color: black;'
		)

		lb_x_coord_title = QLabel('X coordinate:')
		lb_x_coord_title.setStyleSheet(
			'font-size: 12pt;\
			font-family: "DejaVu Sans Mono";'
		)

		lb_y_coord_title = QLabel('Y coordinate:')
		lb_y_coord_title.setStyleSheet(
			'font-size: 12pt;\
			font-family: "DejaVu Sans Mono";'
		)

		lb_z_angle_title = QLabel('Z angle:')
		lb_z_angle_title.setStyleSheet(
			'font-size: 12pt;\
			font-family: "DejaVu Sans Mono";'
		)

		# Grid layout configuration
		ly_grid = QGridLayout()
		ly_grid.setAlignment(Qt.AlignTop)
		ly_grid.setSpacing(5)
		self.setLayout(ly_grid)

		# Grid layout widgets configuration
		ly_grid.addWidget(lb_title, 0, 0, 1, 2)
		ly_grid.addWidget(lb_x_coord_title, 1, 0, 1, 1)
		ly_grid.addWidget(self.lb_x_coord, 1, 1, 1, 1)
		ly_grid.addWidget(lb_y_coord_title, 2, 0, 1, 1)
		ly_grid.addWidget(self.lb_y_coord, 2, 1, 1, 1)
		ly_grid.addWidget(lb_z_angle_title, 3, 0, 1, 1)
		ly_grid.addWidget(self.lb_z_angle, 3, 1, 1, 1)


	def update_coordinates(self, x_coord, y_coord, z_angle):
		self.lb_x_coord.setText("%6.2f cm" % (x_coord / 10.0))
		self.lb_y_coord.setText("%6.2f cm" % (y_coord / 10.0))
		self.lb_z_angle.setText("%6.2f deg" % (z_angle * 180 / 3.14159))
