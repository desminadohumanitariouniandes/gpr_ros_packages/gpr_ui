
import rospy
from gpr_msgs.msg import Coordinates

from coord_info import CoordInfo
from coord_display import CoordDisplay

from PyQt5.QtWidgets import QMainWindow, QWidget, QVBoxLayout, QHBoxLayout


class RobotUserInterface(QMainWindow):

	
	def __init__(self):

		super(RobotUserInterface, self).__init__()
		
		# Subscribe to current coordinates topic
		self._cc_sub = rospy.Subscriber("current_coord", Coordinates, self.current_coord_callback)

		self.setWindowTitle('GPR-20')
		self.setFixedSize(800, 480)

		# Initializing all required widgets
		self.wg_position_display = CoordDisplay()
		self.wg_position_info = CoordInfo()
		# self.wg_vna_status = TODO

		# Right layout setup
		ly_right = QVBoxLayout()
		ly_right.addWidget(self.wg_position_info)

		# Bounding layout setup
		ly_bound = QHBoxLayout()
		ly_bound.addWidget(self.wg_position_display, 6)
		ly_bound.addLayout(ly_right, 4)

		# Main layout definition
		ly_main = QVBoxLayout()
		ly_main.setContentsMargins(5, 5, 5, 5)
		ly_main.addLayout(ly_bound)

		# Set central widget and layout
		wg_main = QWidget()
		wg_main.setStyleSheet('background-color: #A1B3A3;')
		wg_main.setLayout(ly_main)
		self.setCentralWidget(wg_main)


	def current_coord_callback(self, data):
		try:
			# Updates coordinates in info widget
			self.wg_position_info.update_coordinates(data.x_coord, data.y_coord, data.z_angle)

			# Update coordinates in display
			self.wg_position_display.update_plot(data.x_coord, data.y_coord, data.z_angle)
		except AttributeError:
			pass


	def __del__(self):
		del self._cc_sub
