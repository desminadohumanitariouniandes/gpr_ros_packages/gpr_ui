# GPR-20 Robot User Interface Package (gpr_ui)

This repo contains a ROS package that displays an user interface on the robot's touchscreen. It is intended to display the robot's antennae position within the work area and warnings whenever a survey is taking place. Since the robot's touchscreen has a 800x480 resolution, this package will output an user interface with the same resolution.
